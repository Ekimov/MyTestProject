package com.example.igorx.mytestproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.igorx.mytestproject.Logger;
import com.example.igorx.mytestproject.R;
import com.example.igorx.mytestproject.Transaction;
import com.example.igorx.mytestproject.adapters.ListTransactionsAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MyTransactionFragment extends AbstractTabFragment {

    private ListView lvTransactions;
    private ListTransactionsAdapter adapter;
    public static MyTransactionFragment getInstance(Context context) {
        MyTransactionFragment fragment = new MyTransactionFragment();
        fragment.setTitle(context.getString(R.string.my_activity));
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_layout_fragfment, container, false);
        lvTransactions = (ListView) view.findViewById(R.id.lvTransactions);
        adapter = new ListTransactionsAdapter(getActivity());
        lvTransactions.setAdapter(adapter);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            Logger.getInstance().addTransaction(new Transaction(getString(R.string.show_fragment) + " " + getTitle()));
        }
    }

}
