package com.example.igorx.mytestproject.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;

public class AbstractTabFragment extends Fragment {

    private String title;
    Context context;
    protected View view;

    public String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }
}