package com.example.igorx.mytestproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.igorx.mytestproject.R;

import java.io.File;
import java.util.ArrayList;


public class ListFilesAdapter extends BaseAdapter {
    ArrayList<File> files;
    Context context;

    public ListFilesAdapter(Context context,ArrayList<File> files) {
        this.files=files;
        this.context=context;
    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public Object getItem(int position) {
        return files.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        File file = files.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_files_adapter, null);
            viewHolder = new ViewHolder();
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.tvMimeType= (TextView) convertView.findViewById(R.id.tvMimeType);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvTitle.setText(file.getName());
        viewHolder.tvMimeType.setText(getMimeType(file.getPath()));
        return convertView;
    }

    static class ViewHolder {
        private TextView tvTitle, tvMimeType;
    }
    public static String getMimeType(String path) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(path);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
