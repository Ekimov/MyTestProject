package com.example.igorx.mytestproject;

import java.util.ArrayList;
import java.util.Collections;


public class Logger {
    private static Logger logger;
    private ArrayList<Transaction> transactions = new ArrayList<>();

    public static Logger getInstance() {
        if (logger == null) {
            logger = new Logger();
        }
        return logger;
    }

    private Logger() {
    }

    public void addTransaction(Transaction transaction) {
        getTransactions().add(0, transaction);
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }
}
