package com.example.igorx.mytestproject.fragments;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.audiofx.EnvironmentalReverb;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.igorx.mytestproject.Logger;
import com.example.igorx.mytestproject.R;
import com.example.igorx.mytestproject.Transaction;
import com.example.igorx.mytestproject.adapters.ListFilesAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;


public class MyFilesFragment extends AbstractTabFragment {
    public static MyFilesFragment getInstance(Context context) {
        MyFilesFragment fragment = new MyFilesFragment();
        fragment.setTitle(context.getString(R.string.my_files));
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_files_layout,container,false);
        ListView lvFiles = (ListView) view.findViewById(R.id.lvFiles);
        final ArrayList<File> files= findFiles(Environment.getExternalStorageDirectory());
        ListFilesAdapter adapter = new ListFilesAdapter(getActivity(),files);
        lvFiles.setAdapter(adapter);
        lvFiles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType(ListFilesAdapter.getMimeType(files.get(position).getPath()));
                    emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(files.get(position)));

                    Logger.getInstance().addTransaction(new Transaction(getString(R.string.send_file) + " " + files.get(position).getName()));
                    startActivity(emailIntent);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Ошибка выполнения операции", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private ArrayList<File> findFiles(File rootDirectory) {
        ArrayList<File> findFileList = new ArrayList<>();
        File[] files = rootDirectory.listFiles();
        for (File singleFile : files) {
            if (singleFile.isDirectory() && !singleFile.isHidden()) {
                 findFileList.addAll(findFiles(singleFile));
            } else {
                if (checkMediaFiles(singleFile.getName())) {
                    findFileList.add(singleFile);
                }
            }
        }
        return findFileList;
    }

    private boolean checkMediaFiles(String name) {
        if (name.endsWith(".mp3") || name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".mp4") || name.endsWith(".pdf")||name.endsWith(".3gp")) {
            return true;
        }
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            Logger.getInstance().addTransaction(new Transaction(getString(R.string.show_fragment) + " " + getTitle()));
        }
    }

}
