package com.example.igorx.mytestproject.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.igorx.mytestproject.Logger;
import com.example.igorx.mytestproject.R;
import com.example.igorx.mytestproject.Transaction;



import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



public class WebFragment extends AbstractTabFragment implements View.OnClickListener {

    private TextView tvResponse;
    private Button btnSend;
    private EditText etEnterUrl;
    private OkHttpClient httpClient;
    private Handler mHandler;
    private String mMessage;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        httpClient = new OkHttpClient();
        mHandler = new Handler(Looper.getMainLooper());
    }

    public static WebFragment getInstance(Context context) {
        Bundle args = new Bundle();
        WebFragment fragment = new WebFragment();
        fragment.setArguments(args);
        fragment.setContext(context);
        fragment.setTitle(context.getString(R.string.web_fragment));
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.web_layout,container,false);
        initView(view);
        return view;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            Logger.getInstance().addTransaction(new Transaction(getString(R.string.show_fragment) + " " + getTitle()));
        }
    }

    private void initView(View view) {
        tvResponse = (TextView) view.findViewById(R.id.tvResponse);
        btnSend = (Button) view.findViewById(R.id.btnSend);
        etEnterUrl= (EditText) view.findViewById(R.id.etEnterText);
        btnSend.setOnClickListener(this);
    }

    private void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSend:
                try {
                    etEnterUrl.setEnabled(false);
                    run(etEnterUrl.getText().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (IllegalArgumentException ex){
                     Toast.makeText(getActivity(),"Введите корретный url",Toast.LENGTH_SHORT).show();
                    etEnterUrl.setEnabled(true);
                }
        }
    }

    private void run(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();


        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                mMessage = response.body().string();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        tvResponse.setText(mMessage);
                        etEnterUrl.setEnabled(true);
                    }
                });

            }
        });
    }

}
