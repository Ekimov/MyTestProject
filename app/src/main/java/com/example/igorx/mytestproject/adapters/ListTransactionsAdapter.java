package com.example.igorx.mytestproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.igorx.mytestproject.Logger;
import com.example.igorx.mytestproject.R;
import com.example.igorx.mytestproject.Transaction;

import java.util.ArrayList;

public class ListTransactionsAdapter extends BaseAdapter {

    private Context context;
    public ListTransactionsAdapter(Context context) {
        this.context=context;
    }

    @Override
    public int getCount() {
        return Logger.getInstance().getTransactions().size();
    }

    @Override
    public Object getItem(int position) {
        return Logger.getInstance().getTransactions().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        Transaction transaction = Logger.getInstance().getTransactions().get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_transaction_adapter, null);
            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvNameTransaction);
            viewHolder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvName.setText(transaction.getNameTransaction());
        viewHolder.tvTime.setText(transaction.getTime());
        return convertView;
    }

    static class ViewHolder {
        private TextView tvTime, tvName;
    }
}
