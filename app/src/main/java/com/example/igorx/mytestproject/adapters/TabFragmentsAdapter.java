package com.example.igorx.mytestproject.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.igorx.mytestproject.fragments.AbstractTabFragment;
import com.example.igorx.mytestproject.fragments.WebFragment;
import com.example.igorx.mytestproject.fragments.MyTransactionFragment;
import com.example.igorx.mytestproject.fragments.MyFilesFragment;

import java.util.HashMap;
import java.util.Map;

public class TabFragmentsAdapter extends FragmentPagerAdapter {

    private Map<Integer, AbstractTabFragment> tabs;
    private Context context;

    public TabFragmentsAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        initTabsMap(context);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs.get(position).getTitle();
    }

    @Override
    public Fragment getItem(int position) {
        return tabs.get(position);
    }

    @Override
    public int getCount() {
        return tabs.size();
    }

    private void initTabsMap(Context context) {
        tabs = new HashMap<>();
        tabs.put(0, MyFilesFragment.getInstance(context));
        tabs.put(1, MyTransactionFragment.getInstance(context));
        tabs.put(2, WebFragment.getInstance(context));
    }
}
