package com.example.igorx.mytestproject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Transaction {
    private String time;
    private String nameTransaction;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    public Transaction(String nameTransaction) {
        this.nameTransaction=nameTransaction;
        this.time=sdf.format(new Date(System.currentTimeMillis()));
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNameTransaction() {
        return nameTransaction;
    }

    public void setNameTransaction(String nameTransaction) {
        this.nameTransaction = nameTransaction;
    }
}
